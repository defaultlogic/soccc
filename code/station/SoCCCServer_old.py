# Tcp server
# Pass file name for logging

# Inspired by: 
# http://www.binarytides.com/code-chat-application-server-client-sockets-python/
# https://wiki.python.org/moin/UdpCommunication

# Test with:
# nc localhost 5000

import socket, select, datetime, sys

def outputLine(line, fileWriter):
    print line
    if not fileWriter == None:
        fileWriter.write(line + "\n")


if __name__ == "__main__":  
    # Check parameter and create log file if desired
    if len(sys.argv) > 1:
        logFile = open(sys.argv[1], 'w')
    else:
        logFile = None

    # List to keep track of socket descriptors
    CONNECTION_LIST = []
    RECV_BUFFER = 4096 # Advisable to keep it as an exponent of 2
    PORT = 5000
     
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # this has no effect, why ?
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(("0.0.0.0", PORT))
    server_socket.listen(10)
 
    # Add server socket to the list of readable connections
    CONNECTION_LIST.append(server_socket)
 
    print "SoCCC server running on port " + str(PORT)
 
    while 1:
        # Get the list sockets which are ready to be read through select
        read_sockets,write_sockets,error_sockets = select.select(CONNECTION_LIST,[],[])
 
        timestamp = datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")

        for sock in read_sockets:
            #New connection
            if sock == server_socket:
                # Handle the case in which there is a new connection recieved through server_socket
                sockfd, addr = server_socket.accept()
                CONNECTION_LIST.append(sockfd)
                outputLine(timestamp + ";" + str(addr[0]) + ": SoCCC connected", logFile)

                #broadcast_data(sockfd, "[%s:%s] entered room\n" % addr)
             
            #Some incoming message from a client
            else:
                # Data recieved from client, process it
                try:
                    #In Windows, sometimes when a TCP program closes abruptly,
                    # a "Connection reset by peer" exception will be thrown
                    data = sock.recv(RECV_BUFFER)
                    if len(data) == 0:
                        raise Exception('Invalid Data')

                    outputLine(timestamp + ";" + str(addr[0]) + ": " + data.rstrip().lstrip(), logFile)
                 
                except:
                    outputLine(timestamp + ";" + str(addr[0]) + ": SoCCC gone", logFile)                    
                    sock.close()
                    CONNECTION_LIST.remove(sock)
                    continue
     
    server_socket.close()
    if not logFile == None:
        logFile.close()