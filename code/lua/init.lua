-- Load WiFi config from file and connect
dofile("wifi.lua")
wifi.sta.config(ssid, key)
wifi.sta.connect()

-- Print status
tmr.alarm(1, 1000, 1, function()
    if wifi.sta.getip()== nil then
        print("IP unavaiable, Waiting...")
    else
        tmr.stop(1)
    
        print("ESP8266 mode is: " .. wifi.getmode())
        print("The module MAC address is: " .. wifi.ap.getmac())
        print("Config done, IP is "..wifi.sta.getip())

        -- Time startup
        abort = false
        tmr.alarm(0, 5000, 0, startup)
    end
end)

-- Check if abort is not set and execute main program
function startup()
    if abort == true then
        print('Startup aborted')
        return
    end

    print('Executing soccc.lua')
    dofile("soccc.lua")
end
