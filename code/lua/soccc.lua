--- SoCCC Main program
function notifyConnected(socket)
    print('Connected to SoCCCServer...')
    connected = true
end


function notifyDisconnected(socket)
    print('Disconnected from SoCCCServer...')
    connected = false
    --connect()
    restartModule()
end

function connect(socket)
    print('connecting...')
    sk:connect(serverPort, serverIP)
end

-------- Main --------

-- keep alive message
tmr.alarm(1, 1000, 1, function()
    print("SoCCCC running...")
    if connected == true
        sck:send("SoCCCC running...\r\n")
    end
end)

connected = false
sk = net.createConnection(net.TCP, 0) 
sk:on("disconnection", notifyDisconnected)
sk:on("connection", notifyConnected)
connect()

