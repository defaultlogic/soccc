-- Code for accessing MPU6050 module via I2C

-- Set pins
id = 0
sda = 4
scl = 5

-- initialize i2c, set pin1 as sda, set pin2 as scl
i2c.setup(id, sda, scl, i2c.SLOW)

-- read from reg_addr content of dev_addr
function readReg(dev_addr, reg_addr)
    i2c.start(id)
    i2c.address(id, dev_addr ,i2c.TRANSMITTER)
    i2c.write(id,reg_addr)
    i2c.stop(id)
    i2c.start(id)
    i2c.address(id, dev_addr,i2c.RECEIVER)
    c=i2c.read(id,1)
    i2c.stop(id)
    return c
end

-- TODO: change value of status register to wake module up from sleep
function wakeUp(dev_addr)


end


-- TODO: read acc data from module
function readAccData()



end


-- Test code, read status register
-- Reg 6B is status register
reg = readReg(0x68, 0x6B)
print(string.byte(reg))
