-- Test ADC
tmr.alarm(1, 1000, 1, function()
    print(adc.read(0))
end)

-- Test WS2812B
ledPin = 7
r = 0
g = 255
b = 0
rgb = string.char(r, g, b) 
ws2812.writergb(ledPin, 0, 0, 255) -- turn first WS2812B, connected to pin 
