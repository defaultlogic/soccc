#include <ESP8266WiFi.h>
#include <Adafruit_NeoPixel.h>


// Definitions for MPU6050 Board

// I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project
#include "I2Cdev.h"
#include "MPU6050.h"

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
		#include "Wire.h"
#endif

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 accelgyro;
//MPU6050 accelgyro(0x69); // <-- use for AD0 high

int16_t ax, ay, az;
int16_t gx, gy, gz;


// uncomment "OUTPUT_READABLE_ACCELGYRO" if you want to see a tab-separated
// list of the accel X/Y/Z and then gyro X/Y/Z values in decimal. Easy to read,
// not so easy to parse, and slow(er) over UART.
#define OUTPUT_READABLE_ACCELGYRO

// uncomment "OUTPUT_BINARY_ACCELGYRO" to send all 6 axes of data as 16-bit
// binary, one right after the other. This is very fast (as fast as possible
// without compression or data loss), and easy to parse, but impossible to read
// for a human.
//#define OUTPUT_BINARY_ACCELGYRO


// Definitions for WSLED 
#define PIN            D3

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(2, PIN, NEO_GRB + NEO_KHZ800);


#define LED_PIN 13
bool blinkState = false;


// Wifi Settings
const char* ssid     = "33C3-open-legacy";
const char* password = "your-password";
const char* ip = "151.217.205.233";

int value = 0;
WiFiClient client;



void setup() {
	Serial.begin(115200);
	delay(10);

	// We start by connecting to a WiFi network
	Serial.println();
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(ssid);
	
	WiFi.begin(ssid, password);
	
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	Serial.println("");
	Serial.println("WiFi connected");  
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());

			// join I2C bus (I2Cdev library doesn't do this automatically)
		#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
				Wire.begin();
		#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
				Fastwire::setup(400, true);
		#endif

	// initialize serial communication
	// (38400 chosen because it works as well at 8MHz as it does at 16MHz, but
	// it's really up to you depending on your project)
	Serial.begin(38400);

	// initialize device
	Serial.println("Initializing I2C devices...");
	accelgyro.initialize();

	// verify connection
	Serial.println("Testing device connections...");
	Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

	Serial.print("connecting to ");
	Serial.println(ip);

	pixels.begin(); // This initializes the NeoPixel library.
	
	pixels.setPixelColor(0, pixels.Color(25, 122, 5)); // Moderately bright green color.
	pixels.setPixelColor(1, pixels.Color(5, 122, 25)); // Moderately bright green color.

	pixels.show(); //
	
	// Use WiFiClient class to create TCP connections
	const int port = 10000;
	if (!client.connect(ip, port)) {
		Serial.println("connection failed");
		return;
	}
	
}

void loop() {
	sendIMUData();
	++value;
}

void sendIMUData() {
		if (!client.connected()) {
				pixels.setPixelColor(0, pixels.Color(255, 0, 0)); // Moderately bright green color.
				pixels.setPixelColor(1, pixels.Color(255, 0, 0)); // Moderately bright green color.
				pixels.show(); // This sends the updated pixel color to the hardware.
				// If we are not connected reset 
				// TODO Proper handling and reconnect plus buffering
				ESP.restart() ;
	 }
		
		// read raw accel/gyro measurements from device
		accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

		// these methods (and a few others) are also available
		//accelgyro.getAcceleration(&ax, &ay, &az);
		//accelgyro.getRotation(&gx, &gy, &gz);

		#ifdef OUTPUT_READABLE_ACCELGYRO
				// display tab-separated accel/gyro x/y/z values
				Serial.print("a/g:\t");
				Serial.print(ax); Serial.print("\t");
				Serial.print(ay); Serial.print("\t");
				Serial.print(az); Serial.print("\t");
				Serial.print(gx); Serial.print("\t");
				Serial.print(gy); Serial.print("\t");
				Serial.println(gz);
		#endif

		#ifdef OUTPUT_BINARY_ACCELGYRO
				Serial.write((uint8_t)(ax >> 8)); Serial.write((uint8_t)(ax & 0xFF));
				Serial.write((uint8_t)(ay >> 8)); Serial.write((uint8_t)(ay & 0xFF));
				Serial.write((uint8_t)(az >> 8)); Serial.write((uint8_t)(az & 0xFF));
				Serial.write((uint8_t)(gx >> 8)); Serial.write((uint8_t)(gx & 0xFF));
				Serial.write((uint8_t)(gy >> 8)); Serial.write((uint8_t)(gy & 0xFF));
				Serial.write((uint8_t)(gz >> 8)); Serial.write((uint8_t)(gz & 0xFF));
		#endif

		// blink LED to indicate activity
		blinkState = !blinkState;
		//digitalWrite(LED_PIN, blinkState);


		// Transfer data over wifi
		client.print("[");
		client.print(millis()); client.print(";");
		client.print(ax); client.print(";");
		client.print(ay); client.print(";");
		client.print(az); client.print(";");
		client.print(gx); client.print(";");
		client.print(gy); client.print(";");
		client.print(gz); client.print("]");

		// Update LED
		pixels.setPixelColor(0, pixels.Color(25, 122, 5)); // Moderately bright green color.
		pixels.setPixelColor(1, pixels.Color(5, 122, 25)); // Moderately bright green color.
		pixels.show(); // This sends the updated pixel color to the hardware.  
}
