#ifndef _MPU6050_H
#define _MPU6050_H

#include <avr/io.h>

#define MPU6050_PWR1_SLEEP_BIT         6
#define MPU6050_RA_PWR_MGMT_1       0x6B
#define MPU6050_RA_ACCEL_XOUT_H     0x3B

extern void mpu6050_init();
extern void mpu6050_getRawData(int16_t* ax, int16_t* ay, int16_t* az, int16_t* gx, int16_t* gy, int16_t* gz);
extern void mpu6050_output_cur_values();

#endif
