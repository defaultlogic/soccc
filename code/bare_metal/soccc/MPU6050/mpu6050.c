#include <util/delay.h>
#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>

#include "lib/i2cmaster.h"
#include "mpu6050.h"
#include "uart.h"

#define MPU6050_ADDR (0x68 << 1)

volatile uint8_t buffer[14];

void mpu6050_output_cur_values() {
	int16_t ax = 0;
	int16_t ay = 0;
	int16_t az = 0;
	int16_t gx = 0;
	int16_t gy = 0;
	int16_t gz = 0;
	mpu6050_getRawData(&ax, &ay, &az, &gx, &gy, &gz);

	// Output
	char itmp[10];
	ltoa(ax, itmp, 10); uart_putc(' '); uart_puts(itmp); uart_putc(' ');
	ltoa(ay, itmp, 10); uart_putc(' '); uart_puts(itmp); uart_putc(' ');
	ltoa(az, itmp, 10); uart_putc(' '); uart_puts(itmp); uart_putc(' ');
	ltoa(gx, itmp, 10); uart_putc(' '); uart_puts(itmp); uart_putc(' ');
	ltoa(gy, itmp, 10); uart_putc(' '); uart_puts(itmp); uart_putc(' ');
	ltoa(gz, itmp, 10); uart_putc(' '); uart_puts(itmp); uart_putc(' ');
	uart_puts("\r\n");
}
	

/*
 * read bytes from chip register
 */
int8_t mpu6050_readBytes(uint8_t regAddr, uint8_t length, uint8_t *data) {
	uint8_t i = 0;
	int8_t count = 0;
	if(length > 0) {
		//request register
		i2c_start(MPU6050_ADDR | I2C_WRITE);
		i2c_write(regAddr);
		_delay_us(10);

		//read data
		i2c_start(MPU6050_ADDR | I2C_READ);

		for(i=0; i<length; i++) {
			count++;
			if(i==length-1)
				data[i] = i2c_readNak();
			else
				data[i] = i2c_readAck();
		}
		i2c_stop();
	}
	return count;
}

/*
 * write bytes to chip register
 */
void mpu6050_writeBytes(uint8_t regAddr, uint8_t length, uint8_t* data) {
	if(length > 0) {
		//write data
		i2c_start(MPU6050_ADDR | I2C_WRITE);
		i2c_write(regAddr); //reg
		for (uint8_t i = 0; i < length; i++) {
			i2c_write((uint8_t) data[i]);
		}
		i2c_stop();
	}
}

void mpu6050_output_register_value(uint8_t data) {
	char itmp[10];
	itoa(data, itmp, 2); 
	uart_puts(itmp);
	uart_puts("\n\r");
}

void mpu6050_disable_sleep() {
	uint8_t cur_power_mgnt_value;
	mpu6050_readBytes(MPU6050_RA_PWR_MGMT_1, 1, &cur_power_mgnt_value);
	mpu6050_output_register_value(cur_power_mgnt_value);

	cur_power_mgnt_value &= ~((1 << MPU6050_PWR1_SLEEP_BIT));
	mpu6050_output_register_value(cur_power_mgnt_value);
	mpu6050_writeBytes(MPU6050_RA_PWR_MGMT_1, 1, &cur_power_mgnt_value);
}

void mpu6050_init() {
	//init i2c
	i2c_init();
	_delay_us(10);

	//allow mpu6050 chip clocks to start up
	_delay_ms(100);

	//set sleep disabled
	mpu6050_disable_sleep();

	//wake up delay needed sleep disabled
	_delay_ms(10);

	//DEBUG_OUT("-Done\n\r");	
}

void mpu6050_getRawData(int16_t* ax, int16_t* ay, int16_t* az, int16_t* gx, int16_t* gy, int16_t* gz) {
	mpu6050_readBytes(MPU6050_RA_ACCEL_XOUT_H, 14, (uint8_t *)buffer);

    *ax = (((int16_t)buffer[0]) << 8) | buffer[1];
    *ay = (((int16_t)buffer[2]) << 8) | buffer[3];
    *az = (((int16_t)buffer[4]) << 8) | buffer[5];
    *gx = (((int16_t)buffer[8]) << 8) | buffer[9];
    *gy = (((int16_t)buffer[10]) << 8) | buffer[11];
    *gz = (((int16_t)buffer[12]) << 8) | buffer[13];
}

