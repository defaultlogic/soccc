# SoCCC Hardware and Software Repo

Requires ESP SDK / Toolchain to compile. Please ensure exisitence of wifi_config.h in the parent directory when using the bare_metal code. For lua ensure file wifi_config.lua on the ESP module.

Use variables `ssid`, `key`, `serverPort` and `serverIP` for lua.
For C use `#defines` `SSID`, `KEY`, `SERVER_PORT` and `SERVER_IP`.

